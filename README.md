## Table of Contents

1. [What is GitBones?](#what-is-gitbones)
2. [Prerequisites](#prerequisites)
    * [Windows](#windows)
    * [Linux](#linux)
    * [MacOS](#macos)
3. [Installing](#installing)
4. [Getting Started](#getting-started)
    * [GitBones Documentation](#gitbones-documentation)
    * [Searching GitBones Templates](#searching-gitbones-templates)
    * [Creating New Repos from Templates](#creating-new-repos-from-templates)

## What is GitBones?

GitBones is a git host agnostic repository templating solution. The purpose of GitBones is to facilitate GitOps initiatives to create consistent and standard repository directories without the need to manual copy files locally into new repositories.


## Prerequisites

Prior to using PS GitBones please ensure your OS has the appropriate configurations and software installed. PS GitBones requires only `git` and `PowerShell Core` or PowerShell to be installed on the OS.

### Windows

The following are the system configurations requirements for using PSGitBones on Windows OS. It is recommended to use [chocolatey](https://chocolatey.org/) to complete the installation as chocolatey installers will set the PATH needed to correct use applications.

> **Note:** If you are already using git via command-line on Windows, you are probably all set. If you can open up PowerShell or PowerShell Core and can run `git --version` without issue, your system meets all prerequisites and you only need to install the GitBones module.

1. Git.exe Installed
    * [Install Git.exe via Chocolatey](https://chocolatey.org/packages?q=Git)
    * Manual installation instructions and installation download [here](https://git-scm.com/download/win)
        * When manually installing make certain the PATH environment variable is set for git.exe
2. [PowerShell Core](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-windows-powershell?view=powershell-6) or PowerShell Installed.
    * PS GitBones will work with PowerShell versions 3.0 or newer; however if new to PowerShell use PowerShell Core for simplified use.
    * PS Core installation can be completed via [Chocolatey's PowerShell Core](https://chocolatey.org/packages/powershell-core)
    * MS Documentation to install [here](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-windows-powershell?view=powershell-6)

To verify run the `git --version` in the PowerShell Core or PowerShell terminal:

```bash
PS C:\Users\grolston> git --version
git version 2.19.2.windows.1
PS C:\Users\grolston>
```

The version of git may be different from above; however the `git --version` command returns a result showing that it is installed and PATH variable is set.

### Linux

The following are the system prerequisites to getting started with PowerShell GitBones on Linux OS.

1. Git Installed
    * Verify via running `command -v git` which should output the location git application is installed.
    * If not installed use OS specific package management solution to install (e.g yum, dnf, snap, apt-get, etc.)
2. PowerShell Core Installed
    * Reference [MS Docs to install](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-linux?view=powershell-6)
    * Verify PowerShell Core is installed via running `command -v pwsh` which should output the location PowerShell Core application is installed.

#### MacOS

The following are the system prerequisites to getting started with PowerShell GitBones on MacOS.

1. Git Installed
    * Verify via running `command -v git` which should output the location git application is installed.
    * Install git via [brew](https://brew.sh/) if missing
2. PowerShell Core Installed
    * Reference [MS Docs to install](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-macos?view=powershell-6)
        * Install instructions leverage brew so if you just used it to install git then the PowerShell Core install should be simple.
    * Verify PowerShell Core is installed via running `command -v pwsh` which should output the location PowerShell Core application is installed.

## Installing

PS GitBones can be installed via the PowerShell Gallery using PowerShell Core terminal or PowerShell terminal. It is recommended you use PowerShell Core if new to PowerShell as the `Install-Module` cmdlet is setup ready for use in the latest version.

The GitBones module can be found on the PowerShell Gallery [here](https://www.powershellgallery.com/packages/GitBones/1.1.0). It is recommended to install and/or update the module from the PowerShell Gallery.

To install open up terminal and start PowerShell Core:

```bash
[grolston@412a5adbb5ed /]# pwsh
PowerShell 6.1.1
Copyright (c) Microsoft Corporation. All rights reserved.

https://aka.ms/pscore6-docs
Type 'help' to get help.

PS />
```

Run the `Install-Module` cmdlet giving GitBones as the name of the Module. If you are wishing to install the module for all users of the system do not specify the `-Scope` argument; however you will need to be running elevated session to install for all users. If wishing to only use GitBones for the current user, run the example below. You can additionally provide a `-verbose` parameter `Install-Module -Name GitBones -Scope CurrentUser -verbose` for detailed output.

```powershell
PS /> Install-Module -Name GitBones -Scope CurrentUser

Untrusted repository
You are installing the modules from an untrusted repository. If you trust this repository, change its
InstallationPolicy value by running the Set-PSRepository cmdlet. Are you sure you want to install the modules from
'PSGallery'?
[Y] Yes  [A] Yes to All  [N] No  [L] No to All  [S] Suspend  [?] Help (default is "N"): A
PS />
```

> **Note**: If it is your first time using PowerShell Gallery, you will be prompted about if you want to set PSGallery as a trusted repository. Please enter Y or A to continue.

A successful installation will return you to a prompt without any errors. Next verify ability to import the module running the following:

```powershell

PS /> IPMO GitBones

```

Installation is complete. See [Using GitBones]() to get starting using the Git templating service.

## Getting Started

To get started you need to load the module into your sessions. Simply open up PowerShell Core terminal and run: `IPMO GitBones`. Once the module is loaded into the session, you are able to start using. Simple use of the module involves referencing either a user specified template or an available GitBones template found using `BonesList` command.

If needing to verify the PS GitBones module is loaded run the `Get-Module -ListAvailable` command. Output should list the GitBones module as such:

```powershell
PS /> Get-Module -ListAvailable


    Directory: /root/.local/share/powershell/Modules


ModuleType Version    Name                                PSEdition ExportedCommands
---------- -------    ----                                --------- ----------------
Script     1.1.2      GitBones                            Desk      {Set-GitPath, Get-GitBonesTemplate, New-GitBones...


    Directory: /opt/microsoft/powershell/6/Modules

...
```

### GitBones Documentation

For detailed instructions regarding the usage of GitBones visit GitBones documentation at [GitBones Docs]().

### Searching GitBones Templates

GitBones provides a template library for users to quickly reference templated repos without need of specifying a URI. Additionally users are able to submit their own templates to the GitBones Library to assist the community in quickly creating templated repositories. This is a great solution when wanting to provide your user-base quick starts or templated solutions.

> **Note:** Organizations can submit their own repo templates to GitBones to give their users simplified setup of repos that is not dependent searching a specific git host for the quickstarts or templates. Visit [GitBones](https://gitlab.com/GitBones/gitbones) to start submitting your organizations quickstart templates.

Users can search the GitBones template library by running `BonesList` to output all templates to their terminal. For full details of how to filter visit [GitBones User Docs]() website. Below is a basic summary of how to use and get started.

#### Filtering Search Results

GitBones `BonesList` command will return all GitBones repo templates which may not be optimal for users to easily find their specific template. Users can leverage PowerShell piping with the `where-object` cmdlet to filter as needed; however, GitBones provides a simplified method to filter for many common use cases. For full details of how to filter visit [GitBones User Docs]() website. Below is a basic summary of how to use and get started.

**Filter By Author**: Users can filter by the Author of the template. Authors are the original creators of the template and for the most part are the same as the maintainer of the template. Authors can be an organization or an individual. The author is specified when new GitBones templates are submitted.

```powershell
PS > BoneList -Author 'GitLab'

...
```

**Filter By Language**: Users can filter by the primary language of the repository. For example a AWS QuickStart for a CloudFormation VPC project would be listed as *CloudFormation*. Users can use this filter to discover templates based on specific language.

```powershell
PS /> BonesList -Language 'CloudFormation'


Name        : GitLab-Starter
URL         : https://gitlab.com/GitBones/templates/gitlab-starter.git
Description : GitLab starter template with description templates
Maintainer  : GitBones
Author      : GitBones
GitHost     : GitLab
CI          : none
Topic       : General
Language    : CloudFormation
Submodules  : False

Name        : GitLab-CloudFormation
URL         : https://gitlab.com/GitBones/templates/gitlab-cloudformation.git
Description : GitLab CloudFormation starter template with description templates
Maintainer  : GitBones
Author      : GitBones
GitHost     : GitLab
CI          : none
Topic       : AWS CloudFormation
Language    : CloudFormation
Submodules  : False
```

**Filter By Topic**: Users can filter by GitBones Topics. GitBones topics are general subject of the template. The topic could be the specific implementation or use of the repository. For example, GitLab publishes various repositories for GitLab Pages setups. One of these repositories is GitBooks. A user can quickly search for this by running the following:

```powershell
BonesList -Topic "GitBook"
```

Additionally the filter does not require that the entire filter be exact. Example running a filter on topic for GitBook without exact wording:

```powershell
PS /> BonesList -Topic "GitB"


Name        : GitLab Pages GitBook
URL         : https://gitlab.com/pages/gitbook.git
Description : Template project for GitLab Pages GitBook.
Maintainer  : GitLab
Author      : GitLab
GitHost     : GitLab
CI          : GitLab-CI
Topic       : GitBook
Language    : Markdown
Submodules  : False
```

Another Example is to filter the topic on *Hugo* which returns examples results:

```powershell
PS /> BonesList -Topic "Hugo"


Name        : GitLab Pages Hugo
URL         : https://gitlab.com/pages/hugo.git
Description : Template project for GitLab Pages Hugo.
Maintainer  : GitLab
Author      : GitLab
GitHost     : GitLab
CI          : GitLab-CI
Topic       : Hugo
Language    : Markdown
Submodules  : False
```

### Creating New Repos from Templates

**Quick Start**: Using gitbones is simple and the primary use-case is to copy repositories as templates to create new repositories. The following example shows how to use gitbones with a `user specified template` called *mytemplate* copying it to a new git repository called *test*:

```powershell
PS > ipmo gitbones
PS > BonesInit -UserTemplate 'https://gitlab.com/rolston/mytemplate.git' -GitRepoUri 'git@gitlab.com:rolston/test.git'
```

GitBones enables users to initialize new repositories with directory templates enabling consistent setup of new repositories. In creating new repositories users simply call the `BonesInit` and can either specify a GitBones template by referencing `-Bones` parameter or a user template `-UserTemplate`. Reference the examples below for further details.

| Parameter | Required | Description |
| --------- | -------- | ----------- |
| `-GitRepoUri` | Yes | This is the remote host git repository you will be pushing to. The remote repository should be newly created.|
| `-Bones` | Yes - Optional | This is the GitBones Template Name found when searching the GitBones library using `BonesList`. The parameter is required if you are not specifying the `-UserTemplate` parameter.|
|`UserTemplate` | Yes - Optional | Specifies a user template repository. The user template can be HTTPS or SSH and authentication is based on local git operations. This parameter is required if not specifying `-Bones` parameter. |

**Using GitBones Templates**

Example: Using simplified BonesInit alias and Bones parameter to create a repository using a GitBones template *GitLab-CloudFormation* with a target user Git repositories using HTTPS as remote origin.

```powershell
PS > BonesInit -GitRepoUri 'https://gitlab.com/rolston/test.git' -Bones 'GitLab-CloudFormation'
```

**Using User Specified Templates**

Example: Using the user specified template with SSH, create a skeleton git repository from the bones of the template *mytemplate.git* to initialize a new repo *test.git*

```powershell
PS > BonesInit -GitRepoUri 'git@gitlab.com:rolston/test.git' -UserTemplate 'git@gitlab.com:rolston/mytemplate.git'
```

Example: Using the user specified template with HTTPS, create a skeleton git repo from the bones of the template *mytemplate.git* to initialize a new repo *test.git*

```powershell
PS > New-GitBonesRepo -GitRepoUri 'git@gitlab.com:rolston/test.git' -UserTemplate 'https://gitlab.com/rolston/mytemplate.git'
```

## Authors

* **George Rolston** - *Initial work* - [GitLab](https://gitlab.com/rolston) | [GitHub](https://github.com/grolston)

## License

This project is licensed under the Apache License - see the [LICENSE.md](LICENSE.md) file for details
